from flask import Flask, render_template, request

app = Flask(__name__)


'''
TEST FUNCTIONS
'''
@app.route('/test/number/')
def test_number():
    import requests

    return requests.post(
        'http://127.0.0.1:5000/feedback/number/',
        json={'value': 3, 'language': 'python', 'type': 'arguments'}).json()


@app.route('/test/syntax/')
def test_syntax():
    import requests

    return requests.post(
        'http://127.0.0.1:5000/feedback/syntax/',
        json={'value': 'def test(a,b,c):', 'language': 'python'}).json()


@app.route('/test/value/')
def test_value():
    import requests

    return requests.post(
        'http://127.0.0.1:5000/feedback/value/',
        json={'value': 'generate_test_feedback', 'language': 'python'}).json()


'''
WEB PAGES
'''
@app.route('/parser/')
def js_parser():
    return render_template('parser.html')


'''
FEEDBACK GENERATORS
'''
@app.route('/feedback/number/', methods=['POST'])
def number_based_feedback():
    '''
    Input (post data):
    - language: indicates the language of the original data, e.g. python
    - type: what is the origin of the value? e.g. arguments count
    - value: what is the value that we need to evaluate?

    Output:
    - score: a grade from 1 (bad) to 5 (excellent) for the provided input
    '''

    value = int(request.json['value'])
    score = 5

    # Some arbitrary example
    if value > 2:
        score = 4
    if value > 3:
        score = 3
    if value > 4:
        score = 2
    if value > 5:
        score = 1

    return {
        'score': score,
    }


@app.route('/feedback/syntax/', methods=['POST'])
def syntax_feedback():
    '''
    Input (post data):
    - language: indicates the language of the original data, e.g. python
    - value: what is the value (code string) that we need to evaluate?

    Output:
    - score: a grade from 1 (bad) to 5 (excellent) for the provided input
    - valid: a boolean that indicates whether the provided input is valid
    '''

    # Our test implementation only tests Python code on validity
    if request.json['language'] == 'python':
        import ast

        try:
            ast.parse(request.json['value'])
        except SyntaxError as e:
            # We assume that an EOF error only occurs in function or
            # class definition nodes, so we will surpress such an error
            if not 'EOF' in str(e):
                return {'score': 0, 'valid': False}

        return {'score': 5, 'valid': True}

    # We use a score of 3 (neutral) when an implementation is missing
    return {'score': 3, 'valid': True}


@app.route('/feedback/value/', methods=['POST'])
def value_based_feedback():
    '''
    Input (post data):
    - language: indicates the language of the original data, e.g python
    - type: what is the origin of the value? e.g. the name of a function
    - value: what is the value that we need to evaluate?

    Output:
    - score: a grade from 1 (bad) to 5 (excellent) for the provided input
    '''

    from difflib import SequenceMatcher

    # The test implementation looks for the most similar entry in the
    # training set, and simply copies the score we have given to that value
    TRAINING_SET = [
        {'value': 'test', 'score': 1},
        {'value': 'generate_feedback', 'score': 5},
        {'value': 'functieNaam', 'score': 3},
    ]

    # Find similarities and return best match
    similarities = [SequenceMatcher(None, request.json['value'], t['value']).ratio()
                    for t in TRAINING_SET]
    best = max(similarities)

    return {
        'score': TRAINING_SET[similarities.index(best)]['score'],
    }
