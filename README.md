It is recommended to use a virtual environment to run this Python project. For example [pipenv](https://pypi.org/project/pipenv/).

==== INSTALL PROJECT ENVIRONMENT ====

$ pipenv install

$ pipenv shell  # to enter environment


==== START SERVER ====

$ export FLASK_APP=server.py

$ flask run
