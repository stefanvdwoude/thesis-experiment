import ast
import requests


def main():
    # We will use a simple test file as a proof-of-concept file to analyze
    with open('static/test.py', 'r') as source:
        tree = ast.parse(source.read())
        analyzer = Analyzer()
        analyzer.visit(tree)


class Analyzer(ast.NodeVisitor):
    URL_NUMBER = 'http://127.0.0.1:5000/feedback/number/'
    URL_SYNTAX = 'http://127.0.0.1:5000/feedback/syntax/'
    URL_VALUE = 'http://127.0.0.1:5000/feedback/value/'

    def _feedback(self, url, data):
        return requests.post(url, json=data).json()

    def visit_FunctionDef(self, node):
        '''
        Analyze a function definition node
        documentation: https://docs.python.org/3/library/ast.html#abstract-grammar

        Task:
        analyze the following list of properties by extending this parser and
        connecting it to the feedback server using the Analyzer._feedback
        function and the correct endpoints and data.

        - name
        - args:
          - number of arguments
          - for each argument:
            - name
        - syntax of full line

        This task is over when you are able to retrieve feedback from the
        correct endpoints for each of the properties. Be sure to have the
        feedback server running on the same port as the urls provided.

        Hint:
        first be sure to analyze which endpoints you should use for each of the
        properties defined above. Then you will need to figure out how to
        extract all required data from the node object, and don't forget
        that you will need to dig deeper to get all data related to the
        args property.

        In order to send a full line of code you should look into saving the entire
        code string for the parsed file in a variable that you can access. Then you
        are able to use the node.lineo property to your advantage to retrieve a
        specific line of code.
        '''

        raise NotImplementedError


if __name__ == "__main__":
    main()
